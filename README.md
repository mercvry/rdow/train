**RDow::EventTrain** is a dead simple message train.

#### features
- small implementation (87 SLOC)
- wildcard listeners
- channels

#### usage
```ruby
train = RDow::EventTrain.new('example')

# Subscribe in default channel
train.subscribe(:event_name) do | event_name, event_arguments | # equivalent to train.channel(:default).subscribe(...)
	puts "#{event_name} was called with #{event_arguments}"
end

# Subscribe in another channel
train[:channel].subscribe(:event_name) do | event_name, event_arguments |
	# ...
end

# Publish in default channel
train.publish(:event_name, 'blah')

# Publish in another channel
train[:channel].publish(:event_name, 'blah')
```

#### todo
- [x] implement wildcard listeners
- [ ] add `subscribe_once`
