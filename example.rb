require_relative 'main.rb'
require_relative '../test/main.rb'
require "pastel"

pastel = Pastel.new

train = RDow::EventTrain.new

train.subscribe :greet do | name, string_a, string_b |
	puts "Hello, #{string_a}! Welcome to #{string_b}."
end

train.subscribe_first :'*' do | name, *arguments |
	!-> { "called #{name} with #{arguments}" }
end

+-> do
	train.publish! :greet, "Ruby", "the World"
	train.publish! :fake	
end
