module RDow
	class EventTrain
		def initialize
			@channels = Hash.new { |hash, key| hash[key] = Channel.new(key) }
		end

		def channel(channel)
			@channels[channel]
		end

		def [](channel)
			self.channel(channel)
		end

		def []=(channel, event, callback)
			self[:global].subscribe(event, &callback)
		end

		def publish(event, *arguments)
			self[:global].publish(event, *arguments)
		end

		def publish!(event, *arguments)
			self[:global].publish!(event, *arguments)
		end

		def publish?(event)
			self[:global].publish?(event)
		end

		def subscribe(event, &callback)	
			self[:global].subscribe(event, &callback)
		end

		def subscribe_once(event, &callback)
			self[:global].subscribe_once(event, &callback)
		end

		def subscribe_first(event, &callback)
			self[:global].subscribe_first(event, &callback)
		end

		def listeners(event)
			self[:global].listeners(event)
		end

		private

		class Channel
			InvalidEvent = Class.new(NoMethodError)

			attr_reader :name
			
			def initialize(name)
				@name = name
				@listeners = Hash.new { |hash, key| hash[key] = [] }
				@wildcard_listeners = []
			end

			def publish(event, *arguments)
				return false unless publish?(event)
				if @wildcard_listeners && @wildcard_listeners.length
					_emit(@wildcard_listeners, event, arguments, star: true)
				end

				listeners = @listeners[event]
				if listeners && listeners.length
					_emit(listeners, event, arguments)
				end
				true
			end

			def publish!(event, *arguments)
				raise InvalidEvent.new("there is no event \"#{event}\" in the channel") unless publish?(event)
				publish(event, *arguments)
			end

			def publish?(event)
				@listeners.has_key?(event)
			end

			def subscribe(event, &callback)	
				if event == :"*"
					@wildcard_listeners << callback
				else
					@listeners[event] << callback
				end
			end

			def subscribe_first(event, &callback)
				if event == :"*"
					@wildcard_listeners.unshift(callback)
				else
					@listeners[event].unshift(callback)
				end
			end

			def remove(event, callback)
				@listeners[event].delete callback
				@wildcard_listeners.delete(callback)
			end

			def listeners(event)
				(@wildcard_listeners + @listeners[event]).flatten.uniq
			end
			
			def _emit(listeners, event, data, star: false)
				listeners.each do |listener|
					listener.call(event, *data)
				end
			end	
		end
	end
end